var keystone = require('keystone'),
Types = keystone.Field.Types;

/**
 * ImageItem Model
 * ==================
*/

var ImageItem = new keystone.List('ImageItem', {
    map: { name: 'title' },
    autokey: { path: 'slug', from: 'title', unique: true }
});

ImageItem.add({
    title: { type: String, required: true },
    image: { type: Types.CloudinaryImage, autoCleanup: true, required: true, initial: true },
    caption: { type: Types.Textarea, height: 150, initial: true },
    gallery: { type: Types.Relationship, ref: 'Gallery', many: false, required: true, initial: true },
});

// ImageItem.schema.virtual('thumbnail').get(() => {
//     return _.thumbnail(300,300);
// })

// ImageItem.relationship({ ref: 'Gallery', path: 'description' });

ImageItem.register();
