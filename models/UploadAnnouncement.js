var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * UploadAnnouncement Model
 * =============
 */

var UploadAnnouncement = new keystone.List('UploadAnnouncement', {
	map: { name: 'title' },
});

UploadAnnouncement.add({
	title: {
		type: String,
		required: true,
	},
	content: {
		type: Types.Html,
		wysiwyg: true,
		height: 400,
	},
});

UploadAnnouncement.relationship({
	ref: 'ChooseAnnouncement',
	path: 'chooseannouncements',
	refPath: 'announcement',
});


UploadAnnouncement.register();
