var keystone = require('keystone');
var _ = require('lodash');
var Types = keystone.Field.Types;

/**
 * Cottage Model
 * ==========
 */

var Cottage = new keystone.List('Cottage', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
	// defaultSort: '-publishedDate'
});

Cottage.add({
	title: { type: String, required: true },
	subtitle: { type: String },
	image: { type: Types.CloudinaryImage },
	description: { type: String },
	facilities: { type: Types.TextArray },
});


Cottage.defaultColumns = 'title, description';
Cottage.register();
