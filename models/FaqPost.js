var keystone = require('keystone');
var _ = require('lodash');
var Types = keystone.Field.Types;

/**
 * FaqPost Model
 * ==========
 */

var FaqPost = new keystone.List('FaqPost', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

FaqPost.add({
	title: { type: String, required: true },
	// state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
	// author: { type: Types.Relationship, ref: 'User', index: true },
	// publishedDate: { type: Types.Datetime },
	// image: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
});

FaqPost.register();
