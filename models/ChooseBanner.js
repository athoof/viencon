var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ChooseBanner Model
 * =============
 */

var ChooseBanner = new keystone.List('ChooseBanner', {
	autokey: { from: 'name', path: 'key', unique: true },
	// nocreate: true,
	nodelete: true,
});

ChooseBanner.add({
	name: { type: String, required: true },
	banner: { type: Types.Relationship, ref: 'UploadBanner' },
});

ChooseBanner.register();
