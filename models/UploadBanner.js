var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * UploadBanner Model
 * =============
 */

var UploadBanner = new keystone.List('UploadBanner', {
	autokey: { from: 'name', path: 'key', unique: true },
});

UploadBanner.add({
	name: { type: String, required: true },
	image: { type: Types.CloudinaryImage },
});

UploadBanner.register();
