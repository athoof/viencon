var keystone = require('keystone');
var _ = require('lodash');
var Types = keystone.Field.Types;
const keystone_namegen = require('keystone-storage-namefunctions');

/**
 * Storage Adapter
 * ===============
 */

var engConditions = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: 'uploads',
        publicPath: '/public/uploads',
        generateFilename: keystone_namegen.originalFilename,
    }
});

var nlConditions = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: 'uploads',
        publicPath: '/public/uploads',
        generateFilename: keystone_namegen.originalFilename,
    }
});

var engRegistration = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: 'uploads',
        publicPath: '/public/uploads',
        generateFilename: keystone_namegen.originalFilename,
    }
});

var nlRegistration = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: 'uploads',
        publicPath: '/public/uploads',
        generateFilename: keystone_namegen.originalFilename,
    }
});

/**
 * DealerInfo Model
 * ==========
 */

var DealerInfo = new keystone.List('DealerInfo', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
	// defaultSort: '-publishedDate'
});

DealerInfo.add({
    title: { type: String },
	Header: { type: Types.Html, wysiwyg: true, height: 200 },
	Content: { type: Types.Html, wysiwyg: true, height: 400 },
	EnglishDealerConditions: { type: Types.File, storage: engConditions },
	DutchDealerConditions: { type: Types.File, storage: nlConditions },
	EnglishRegistrationForm: { type: Types.File, storage: engRegistration },
	DutchRegistrationForm: { type: Types.File, storage: nlRegistration },
});


DealerInfo.defaultColumns = 'title, description';
DealerInfo.register();
