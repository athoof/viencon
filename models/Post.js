var keystone = require('keystone');
var _ = require('lodash');
var Types = keystone.Field.Types;
var striptags = require('striptags');

const { format } = require('timeago.js');

/**
 * Post Model
 * ==========
 */

var Post = new keystone.List('Post', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
	defaultSort: '-publishedDate'
});

Post.add({
	title: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
	author: { type: Types.Relationship, ref: 'User', index: true },
	publishedDate: { type: Types.Datetime },
	image: { type: Types.CloudinaryImage },
	content: { type: Types.Html, wysiwyg: true, height: 400 },
	categories: { type: Types.Relationship, ref: 'PostCategory', many: true },
});

Post.schema.virtual('brief').get(function () {
	let brief = _.truncate(striptags(this.content), {
		length: 100,
		separator: /,?\.* +/,
	});
	return brief;
});

Post.schema.virtual('formatted').get(function () {
	var t = format(this.publishedDate, 'en_US');
	return t;
});

Post.defaultColumns = 'title|30%, state|10%, author|10%, categories|20%, publishedDate|20%';
Post.register();
