var keystone = require('keystone'),
Types = keystone.Field.Types;

/**
 * Gallery Model
 * =============
 */

var Gallery = new keystone.List('Gallery', {
    map: { name: 'name' },
    autokey: { path: 'slug', from: 'name', unique: true }
});

Gallery.add({
    name: { type: String, required: true},
    description: { type: Types.Textarea, height: 150 },
    // ImageItem : { type: Types.Relationship, ref: 'ImageItem', many: true }
});

Gallery.relationship({
	ref: 'ImageItem',
	path: 'description'
});

Gallery.defaultColumns = 'name, description';
Gallery.register();

// var keystone = require('keystone');
// var Types = keystone.Field.Types;

// /**
//  * Gallery Model
//  * =============
//  */

// var Gallery = new keystone.List('Gallery', {
// 	autokey: { from: 'name', path: 'key', unique: true },
// });

// Gallery.add({
// 	name: { type: String, required: true },
// 	publishedDate: { type: Date, default: Date.now },
// 	heroImage: { type: Types.CloudinaryImage },
// 	images: { type: Types.CloudinaryImages },
// });

// Gallery.schema.virtual('thumbnail').get(function () {
// 	let thumbnail = _.thumbnail(600, 600);
// 	return thumbnail;
// });

// Gallery.schema.virtual('limit').get(function (width, height) {
// 	let limit = _.limit(width, height);
// 	return limit;
// });

// Gallery.register();
