var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ChooseAnnouncement Model
 * =============
 */

var ChooseAnnouncement = new keystone.List('ChooseAnnouncement', {
	autokey: { from: 'name', path: 'key', unique: true },
	nocreate: true,
	nodelete: true,
});

ChooseAnnouncement.add({
	name: { type: String, required: true },
	announcement: { type: Types.Relationship, ref: 'UploadAnnouncement' },
});

ChooseAnnouncement.register();
