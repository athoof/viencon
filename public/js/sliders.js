var eventGlide = new Glide('.EventSlider', {
	type: 'carousel',
	hoverpause: true,
	autoplay: 4250,
	focusAt: 'center',
	animationTimingFunc: 'ease-in-out',
	animationDuration: 750,
	perTouch: 1,
	touchRatio: 1.25,
	arrowsWrapperClass: 'slider-arrows',
	arrowRightText: '',
	arrowLeftText: '',
});
var guestGlide = new Glide('.GuestSlider', {
	type: 'carousel',
	hoverpause: true,
	autoplay: 5250,
	focusAt: 'center',
	animationTimingFunc: 'ease-in-out',
	animationDuration: 750,
	perTouch: 1,
	touchRatio: 1.25,
	arrowsWrapperClass: 'slider-arrows',
	arrowRightText: '',
	arrowLeftText: '',
});

eventGlide.mount()
guestGlide.mount()

$(window).trigger('resize', {})
window.dispatchEvent(new Event('resize'));

// setInterval(() => {
// 	glide.go('>');
// }, 5250);

/* $('.slides').glide({
	autoplay: false,
	time: 5500,
	type: 'carousel',
	arrowsWrapperClass: 'slider-arrows',
	arrowRightText: '',
	arrowLeftText: '',
	perView: 1,
})
 */