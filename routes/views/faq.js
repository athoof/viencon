var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Init locals
	locals.section = 'faq';
	locals.filters = {
		tab: req.params.tab,
	};


/* 	// Load all categories
	view.on('init', function (next) {

		keystone.list('PostCategory').model.find().sort('name').exec(function (err, results) {

			if (err || !results.length) {
				return next(err);
			}

			locals.data.categories = results;

			// Load the counts for each category
			async.each(locals.data.categories, function (category, next) {

				keystone.list('Post').model.count().where('categories').in([category.id]).exec(function (err, count) {
					category.postCount = count;
					next(err);
				});

			}, function (err) {
				next(err);
			});
		});
	}); */

	// Load the current category filter
	view.on('init', function (next) {
		if (req.params.tab) {
			locals.tab = req.params.tab;
		} else {
			locals.tab = 'general';
		}
		keystone.list('FaqPost').model.findOne({ slug: locals.tab }).exec(function (err, result) {
			locals.faqpost = result;
			// console.log(result);
			next(err);
		});
	});

	view.query('allFaq', keystone.list('FaqPost').model.find());

/* 	// Load the posts
	view.on('init', function (next) {

		var q = keystone.list('Post').paginate({
			page: req.query.page || 1,
			perPage: 10,
			maxPages: 10,
			filters: {
				state: 'published',
			},
		})
			.sort('-publishedDate')
			.populate('author categories');

		if (locals.data.category) {
			q.where('categories').in([locals.data.category]);
		}

		q.exec(function (err, results) {
			locals.data.posts = results;
			next(err);
		});
	}); */

	// Render the view
	view.render('faq');
};
