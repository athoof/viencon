var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'gallery';
	locals.galleries = [];

	// Load the galleries by sortOrder

	view.on('init', function (next) {
		var q = keystone.list('ImageItem').model.find().sort('sortOrder').populate('gallery');

		q.exec(function (err, results) {
			results.forEach((imageItem) => {
				if (imageItem.image != null && typeof imageItem.image !== 'undefined') {
					imageItem.thumbnail = imageItem._.image.thumbnail(300,300);
				} else {
					imageItem.image.source_url = null;
					imageItem.thumbnail = null;
				}
			});
			locals.galleries = Object.values(results.reduce((res, {name, image, description, gallery, thumbnail, caption, title}) => {
				if (gallery.name != "EventSlider" && gallery.name != "GuestSlider") {
					if (!res[gallery]) res[gallery] = {
						name: gallery.name,
						description: gallery.description,
						images: [],
					};
					res[gallery].images.push({
						image,
						thumbnail,
						caption,
						title,
					});
				}
				console.log(res);
				return res
			}, {}));
			next(err);
		});
	});
	// Render the view
	view.render('gallery');

};
