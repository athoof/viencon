var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);


	var locals = res.locals;
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	view.query('newsfeedItems', keystone.list('Post').model.find().populate('author categories').sort('-publishedDate'));
	view.on('init', function (next) {
		var guestQ = keystone.list('Gallery').model.findOne({name: 'GuestSlider'}).exec();
		var eventQ = keystone.list('Gallery').model.findOne({name: 'EventSlider'}).exec();
		guestQ.then((guestID) => {//Get GuestSlider ID, and then find images in this gallery
			guestQ2 = keystone.list('ImageItem').model.find().where('gallery').in([guestID]).exec();
				guestQ2.then((res) => {
					locals.guestslider = res;
					// console.log(res);
				});
				guestQ2.catch((err) => {
					next(err);
				});
		});
		eventQ.then((eventID) => {//Get EventSlider ID, and then find images in this gallery
			eventQ2 = keystone.list('ImageItem').model.find().where('gallery').in([eventID]).exec();
				eventQ2.then((res) => {
					locals.eventslider = res;
					// console.log(res);
				});
				eventQ2.catch((err) => {
					next(err);
				});
			});
		Promise.all([guestQ, eventQ]).then((values) => {
			// console.log(values);
			next();
		});
	});
		// Render the view
	view.render('index');
};
