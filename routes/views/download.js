var keystone = require('keystone');

exports = module.exports = (req, res) => {

    var view = new keystone.View(req, res);
    var locals = res.locals;

    var q = keystone.list('DealerInfo').model.findOne();

    if (req.params.file) {
        // var file = __dirname + '../uploads/' + req.params.file;
        q.exec((err, result) => {
            if (err) throw err;
            console.log(result.path)
            let file = result.EnglishDealerConditions
            res.sendFile(file);
        })
        // console.log(file)
    } else {
        res.status(404).send('Cannot access this file');
    }
};